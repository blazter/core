# After the Import
After the import following configuration needs to be modified:
- [ ] Modify *Settings/CI-CD/General pipelines/CI/CD configuration file* to *.gitlab/Pipeline.yaml*
- [ ] Modify *Settings/Repository/Branch Defaults/Branch name template* to *feature/%{id}-%{title}*
- [ ] Import the project to [SonarCloud](https://sonarcloud.io/projects)
- [ ] Modify the *#PROJECT_KEY#* placeholder to Sonar Project Key on the *Develop* branch in the *.gitlab/Pipeline.yaml*
- [ ] Modify the *#PROJECT_KEY#* placeholder to Sonar Project Key on the *Main* branch in the *.gitlab/Pipeline.yaml*
