﻿namespace Blazter.Test.Unit.Components.Models
{
    internal record TestOption
    {
        public int Id { get; init; }
        public string Name { get; init; } = null!;
    }
}
