﻿using AngleSharp.Dom;
using Blazter.Test.Unit.Components.Models;

namespace Blazter.Test.Unit.Components.Helpers
{
	internal static class Extensions
	{
		public static IEnumerable<Action<IElement>> AsInspectors(this IEnumerable<string> options)
		{
			foreach (var option in options)
			{
				yield return e => Assert.Equal(option, e.TextContent);
			}
		}

		public static IEnumerable<Action<IElement>> AsDropDownInspectors(this IEnumerable<TestOption> options)
		{
			foreach (var option in options)
			{
				yield return e =>
				{
					Assert.Equal(option.Id.ToString(), e.GetAttribute("value"));
					Assert.Equal(option.Name, e.TextContent);
				};
			}
		}

		public static IEnumerable<Action<IElement>> AsRadioButtonInspectors(this IEnumerable<TestOption> options)
		{
			foreach (var option in options)
			{
				yield return e => Assert.Equal(option.Name, e.TextContent);
			}
		}
	}
}
