﻿using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Blazter.Test.Unit")]
namespace Blazter.Core.Helpers
{
	internal static class Extensions
	{
		public static string? GetValue(this object value, string property)
		{
			return value?.GetType().GetProperty(property)?.GetValue(value)?.ToString();
		}
	}
}
