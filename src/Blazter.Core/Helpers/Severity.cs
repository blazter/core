﻿namespace Blazter
{
	public enum Severity
	{
		Primary,
		Secondary,
		Danger
	}
}
